using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

// The base class for the AI
public abstract class AI : MonoBehaviour
{
    protected TileManager[] m_Tiles;
    protected Vector2Int m_GridSize;
    protected Vector2Int m_CurrentTile;
    protected Vector2Int m_TargetTile;
    protected List<Vector2Int> m_Path;
    protected Coroutine m_MoveCoroutine;


    // Some conbinient properties
    public Vector2Int CurrentTile => m_CurrentTile;
    public Vector2Int TargetTile => m_TargetTile;
    public bool IsMoving => m_MoveCoroutine != null;

    // Setup the data for the AI to work things out
    public virtual void Setup(Vector2Int tile, Vector2Int gridSize, TileManager[] tiles)
    {
        m_CurrentTile = tile;
        m_GridSize = gridSize;
        m_Tiles = tiles;
        m_Path = null;
        m_MoveCoroutine = null;
    }

    // Set the target tile for the AI to move to
    // and auto schedule the move coroutine
    public void SetTarget(Vector2Int target)
    {
        if (target == m_TargetTile)
        {
            return;            
        }
        CancelMove();
        m_TargetTile = target;
        UpdatePath();
    }

    // Find the path to the target tile
    protected void UpdatePath()
    {
        m_Path = AStarSearch.FindPath(m_Tiles, m_CurrentTile, m_TargetTile, m_GridSize.x, m_GridSize.y);
        if (m_Path == null || m_Path.Count == 0)
        {
            Debug.LogWarning("No path found to move to the tile");
            return;
        }
        m_MoveCoroutine = StartCoroutine(MoveCoroutine());
    }

    // Move the AI to the target tile
    IEnumerator MoveCoroutine()
    {
        if (m_Path != null)
        {
            // remove the first tile from the path as it is the current tile
            m_Path.RemoveAt(0);
        }

        // move to the target tile
        foreach (var tile in m_Path)
        {
            var tileMan = m_Tiles[tile.y * m_GridSize.x + tile.x];

            // a callback to customize the movement behaviour
            if (!this.OnNewTile(tile)) break;

            var targetPosition = new Vector3(tileMan.transform.position.x, transform.position.y, tileMan.transform.position.z);

            // move till we get very close to our destination
            while (Vector3.Distance(transform.position, targetPosition) > 0.1f)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.deltaTime * 5f);
                yield return null;
            }

            m_CurrentTile = tile;
        }
        m_MoveCoroutine = null;
    }

    // Cancel the ongoing move
    public void CancelMove()
    {
        if (m_MoveCoroutine != null)
        {
            StopCoroutine(m_MoveCoroutine);
            m_MoveCoroutine = null;
        }
    }

    public abstract bool OnNewTile(Vector2Int tile);
}
