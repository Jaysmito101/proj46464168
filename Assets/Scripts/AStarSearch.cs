using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Node
{
    public int X { get; set; }
    public int Y { get; set; }
    public double G { get; set; } // Cost from start to this node
    public double H { get; set; } // Heuristic cost from this node to target
    public double F => G + H; // Total cost
    public Node Parent { get; set; }

    public Node(int x, int y)
    {
        X = x;
        Y = y;
    }
}

public class AStarSearch
{
    private static readonly Vector2Int[] Directions =
    {
        new Vector2Int(0, 1),
        new Vector2Int(1, 0),
        new Vector2Int(0, -1),
        new Vector2Int(-1, 0)
    };

    public static List<Vector2Int> FindPath(TileManager[] map, Vector2Int start, Vector2Int target, int width, int height)
    {
        var openList = new List<Node>();
        var closedList = new HashSet<Node>();
        var startNode = new Node(start.x, start.y);
        var targetNode = new Node(target.x, target.y);

        openList.Add(startNode);

        while (openList.Count > 0)
        {
            var currentNode = openList.OrderBy(node => node.F).First();

            if (currentNode.X == targetNode.X && currentNode.Y == targetNode.Y)
            {
                return RetracePath(currentNode);
            }

            openList.Remove(currentNode);
            closedList.Add(currentNode);

            foreach (var direction in Directions)
            {
                var newX = currentNode.X + direction.x;
                var newY = currentNode.Y + direction.y;

                if (!IsValidPosition(map, newX, newY, width, height) || closedList.Any(node => node.X == newX && node.Y == newY))
                {
                    continue;
                }

                var neighborNode = new Node(newX, newY)
                {
                    G = currentNode.G + 1,
                    H = GetHeuristic(newX, newY, targetNode.X, targetNode.Y),
                    Parent = currentNode
                };

                if (openList.Any(node => node.X == newX && node.Y == newY && node.G <= neighborNode.G))
                {
                    continue;
                }

                openList.Add(neighborNode);
            }
        }

        return new List<Vector2Int>(); // No path found
    }

    private static bool IsValidPosition(TileManager[] map, int x, int y, int width, int height)
    {
        return x >= 0 && y >= 0 && x < width && y < height && map[y * width + x].Type != TileType.Obstacle;
    }

    private static double GetHeuristic(int x1, int y1, int x2, int y2)
    {
        return Math.Abs(x1 - x2) + Math.Abs(y1 - y2);
    }

    private static List<Vector2Int> RetracePath(Node endNode)
    {
        var path = new List<Vector2Int>();
        var currentNode = endNode;

        while (currentNode != null)
        {
            path.Add(new Vector2Int(currentNode.X, currentNode.Y));
            currentNode = currentNode.Parent;
        }

        path.Reverse();
        return path;
    }
}
