using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;

[CustomEditor(typeof(ObstaclesData))]
public class ObstaclesDataEditor : Editor
{
    public override void OnInspectorGUI()
    {
        ObstaclesData data = (ObstaclesData)target;
        var oldGridSize = data.gridSize;

        EditorGUILayout.LabelField("Obstacles Cutom Editor", EditorStyles.boldLabel);
        EditorGUILayout.Space();

        // A vector 2 field for the size of the obstacles
        data.gridSize = EditorGUILayout.Vector2IntField("Grid Size", data.gridSize);

        if (oldGridSize != data.gridSize || data.gridTiles == null)
        {
            data.gridTiles = new TileType[data.gridSize.x * data.gridSize.y];
            for (int i = 0; i < data.gridTiles.Length; i++)
            {
                data.gridTiles[i] = TileType.Empty;
            }
        }

        if (data.gridSize.x <= 0 || data.gridSize.y <= 0)
        {
            EditorGUILayout.Space();
            EditorGUILayout.HelpBox("Grid size must be greater than 0", MessageType.Error);
            return;
        }

        if (GUILayout.Button("Reset Grid"))
        {
            for (int i = 0; i < data.gridTiles.Length; i++)
            {
                data.gridTiles[i] = TileType.Empty;
            }
        }


        // A grid of buttons to select the type of tile
        for (int y = 0; y < data.gridSize.y; y++)
        {
            EditorGUILayout.BeginHorizontal();
            for (int x = 0; x < data.gridSize.x; x++)
            {
                var prevColor = GUI.backgroundColor;
                if (data.GetTile(x, y) == TileType.Empty) 
                    GUI.backgroundColor = Color.white;
                else
                    GUI.backgroundColor = Color.black;

                if (GUILayout.Button("", GUILayout.Width(20), GUILayout.Height(20)))
                {
                    if (data.GetTile(x, y) == TileType.Empty)
                        data.SetTile(x, y, TileType.Obstacle);
                    else
                        data.SetTile(x, y, TileType.Empty);
                }

                GUI.backgroundColor = prevColor;

            }
            EditorGUILayout.EndHorizontal();
        }


    }

}
