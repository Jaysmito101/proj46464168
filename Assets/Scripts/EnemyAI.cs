using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAI : AI
{
    private AI m_Player;

    public void Setup(Vector2Int tile, Vector2Int gridSize, TileManager[] tiles, AI player)
    {
        m_Player = player;


        // call the base class setup
        base.Setup(tile, gridSize, tiles);
    }

    // A helper function to check if the player is on the neighbour tile
    bool IsPlayerOnNeighbourTile()
    {
        var tile = m_CurrentTile;

        // get the neighbour tiles (on 4 directions)
        var neighbourTiles = new Vector2Int[]
        {
            new Vector2Int(tile.x, tile.y + 1),
            new Vector2Int(tile.x, tile.y - 1),
            new Vector2Int(tile.x + 1, tile.y),
            new Vector2Int(tile.x - 1, tile.y)
        };

        // check if the player is on the neighbour tile
        foreach (var neighbour in neighbourTiles)
        {
            if (neighbour == m_Player.CurrentTile)
            {
                return true;
            }
        }

        return false;
    }

    public void Update()
    {
        // if the player is not on the neighbour tile, then move towards the player
        if (!IsPlayerOnNeighbourTile())
        {
            // this auto ignores the case when the player is on the same tile
            SetTarget(m_Player.CurrentTile);
        }
    }


    public override bool OnNewTile(Vector2Int tile)
    {
        // check if the player is on the neighbour tile
        // if the enemy is on the neighbour tile, then don't move
        if (IsPlayerOnNeighbourTile()) return false;

        var tileMan = m_Tiles[tile.y * m_GridSize.x + tile.x];

        // update the color for a trail
        tileMan.SetColor(Color.yellow);

        return true;
    }

}
