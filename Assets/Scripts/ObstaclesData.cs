using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/ObstaclesData", order = 1)]
public class ObstaclesData : ScriptableObject
{
    public Vector2Int gridSize;
    public TileType[] gridTiles;

    public TileType GetTile(int x, int y)
    {
        if (x < 0 || x >= gridSize.x || y < 0 || y >= gridSize.y)
        {
            return TileType.Obstacle;
        }
        return gridTiles[x + y * gridSize.x];
    }

    public void SetTile(int x, int y, TileType type)
    {
        if (x < 0 || x >= gridSize.x || y < 0 || y >= gridSize.y)
        {
            return;
        }
        gridTiles[x + y * gridSize.x] = type;
    }
}