using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor;
using UnityEngine;

public class ObstaclesManager : MonoBehaviour
{
    [SerializeField] private GameObject playerPrefab;
    [SerializeField] private GameObject enemyPrefab;
    [SerializeField] private GameObject gridTilePrefab;
    [SerializeField] private GameObject gridContainer;
    [SerializeField] private TMP_Text tileInfoUI;
    [SerializeField] private ObstaclesData gridData;

    private TileManager m_LastHoveredTile = null;
    private bool m_IsGridGenerated = false;
    private TileManager[] m_GridTiles = null;
    private List<EnemyAI> m_Enemies = new List<EnemyAI>();
    private PlayerController m_Player;

    // Update is called once per frame
    void Update()
    {
        // Check for tile input
        HighlightHoveringTile();
    }

    // Destroy the current grid and free the resource
    public void DestroyGrid()
    {
        if (m_GridTiles != null)
        {
            foreach (var tile in m_GridTiles)
            {
                Destroy(tile.gameObject);
            }
            m_GridTiles = null;
        }
        for (int i = 0; i < m_Enemies.Count; i++)
        {
            Destroy(m_Enemies[i].gameObject);
        }
        m_Enemies.Clear();
        Destroy(m_Player?.gameObject);
        m_Player = null;
        m_IsGridGenerated = false;
    }

    // Ssearches and returns a random empty tile
    private TileManager GetRandomEmptyTile()
    {
        // find a random empty tile
        var emptyTiles = new List<TileManager>();
        foreach (var tile in m_GridTiles)
        {
            if (tile.Type == TileType.Empty)
            {
                emptyTiles.Add(tile);
            }
        }

        if (emptyTiles.Count == 0)
        {
            Debug.LogError("No empty tile found to drop the player");
            return null;
        }

        return emptyTiles[Random.Range(0, emptyTiles.Count)];
    }

    // Randomly drop the player on any empty tile
    public void DropPlayer()
    {
        if (!m_IsGridGenerated)
        {
            Debug.LogWarning("Grid is not generated yet. Cannot drop player.");
            return;
        }

        if (m_Player != null)
        {
            Debug.LogWarning("Player is already dropped. Destroying it.");
            Destroy(m_Player);
        }

        var randomTile = GetRandomEmptyTile();

        if (randomTile == null) return;
        
        var player = Instantiate(playerPrefab, randomTile.transform.position, Quaternion.identity);
        player.transform.position += Vector3.up * 1.5f;

        m_Player = player.GetComponent<PlayerController>();
        m_Player.Setup(randomTile.GridPosition, gridData.gridSize, m_GridTiles);
    }

    // Randomly drop an ennemy on any empty tile
    public void DropEnemy()
    {
        if (!m_IsGridGenerated)
        {
            Debug.LogWarning("Grid is not generated yet. Cannot drop enemy.");
            return;
        }

        if (m_Player == null)
        {
            Debug.LogWarning("Player is not dropped yet. Cannot drop enemy.");
            return;
        }

        var emptyTiles = new List<TileManager>();
        foreach (var tile in m_GridTiles)
        {
            if (tile.Type == TileType.Empty)
            {
                emptyTiles.Add(tile);
            }
        }

        if (emptyTiles.Count == 0)
        {
            Debug.LogError("No empty tile found to drop the enemy");
            return;
        }

        var randomTile = emptyTiles[Random.Range(0, emptyTiles.Count)];
        var enemy = Instantiate(enemyPrefab, randomTile.transform.position + Vector3.up, Quaternion.identity);
        var enemyAi = enemy.GetComponent<EnemyAI>();
        enemyAi.Setup(randomTile.GridPosition, gridData.gridSize, m_GridTiles, m_Player);
        m_Enemies.Add(enemyAi);
    }

    // Generate the grid and objects for it
    public void GenerateGrid()
    {
        if (m_IsGridGenerated)
        {
            Debug.LogWarning("Grid is already generated! Destroying it.");
            DestroyGrid();
        }


        if (gridTilePrefab == null)
        {
            Debug.LogError("Grid Tile Prefab is not assigned");
            return;
        }   

        if (gridContainer == null)
        {
            Debug.LogError("Grid Container is not assigned");
            return;
        }

        if (gridData.gridSize.x <= 0 || gridData.gridSize.y <= 0)
        {
            Debug.LogError("Grid Size is invalid");
            return;
        }

        m_GridTiles = new TileManager[gridData.gridSize.x * gridData.gridSize.y];

        for (int i = 0; i < gridData.gridSize.y; i++)
        {
            for (int j = 0; j < gridData.gridSize.x; j++)
            {
                // * 1.05f is to create a small gap between the tiles just for visual clarity as I am not using any material for the tiles
                GameObject gridTile = Instantiate(gridTilePrefab, new Vector3(i, 0, j) * 1.05f, Quaternion.identity);
                gridTile.transform.SetParent(gridContainer.transform);

                TileManager tileManager = gridTile.GetComponent<TileManager>();
                if (tileManager != null)
                {
                    tileManager.SetupTile(j, i, gridData.GetTile(j, i));
                    m_GridTiles[i * gridData.gridSize.x + j] = tileManager;
                }
            }
        }

        m_IsGridGenerated = true;
    }

    // The main check to update the hovered tile
    private void HighlightHoveringTile()
    {
        if (!m_IsGridGenerated) return;

        // Get mouse position
        var mousePosition = Input.mousePosition;

        // raycast 
        Ray ray = Camera.main.ScreenPointToRay(mousePosition);
        RaycastHit hitInfo;


        if (Physics.Raycast(ray, out hitInfo))
        {
            var hitObject = hitInfo.collider.gameObject;
            if (hitObject != null)
            {
                TileManager tileManager = hitObject.GetComponent<TileManager>();
                // if a correct object is hit then update it
                if (tileManager != null && tileManager.IsReady)
                {
                    tileInfoUI.text = tileManager.GetInfo();
                    if (m_LastHoveredTile != null) m_LastHoveredTile.SetHovered(false);
                    tileManager.SetHovered(true);
                    m_LastHoveredTile = tileManager;

                    // if clicked try to move player to it
                    if (Input.GetMouseButton(0) 
                        && tileManager.Type == TileType.Empty) 
                        m_Player?.MoveToTile(tileManager);
                }
            }
        } 
        else
        {
            // Not really necessary but lookss better
            tileInfoUI.text = "Hover on tile to see info.";

            // reset hovered tile to update its color
            if (m_LastHoveredTile != null)
            {
                m_LastHoveredTile.SetHovered(false);
                m_LastHoveredTile = null;
            }
        }


    }


}
