using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : AI
{
    // Move the player to any tile
    public void MoveToTile(TileManager tile)
    {
        // Cancel the current move if the player is already moving
        if (IsMoving) return;

        // Set the target tile  
        SetTarget(tile.GridPosition);
    }

    // Cancel the current move if the player is already moving
    public override bool OnNewTile(Vector2Int tile)
    {
        var tileMan = m_Tiles[tile.y * m_GridSize.x + tile.x];
        tileMan.SetColor(Color.green);
        return true;
    }

}
