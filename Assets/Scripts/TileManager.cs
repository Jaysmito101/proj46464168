using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public enum TileType
{
    Empty,
    Obstacle,
}

public class TileManager : MonoBehaviour
{
    [SerializeField] private GameObject obstacleObject;
    [SerializeField] private MeshRenderer meshRenderer;
    [SerializeField] private Color hoveredColor = Color.gray;

    private Vector2Int m_GridPosition = Vector2Int.zero;
    private bool m_IsReady = false;
    private TileType m_TileType = TileType.Empty;
    private string m_TileName = "Tile";
    private bool m_IsHovered = false;
    private Color m_OriginalColor = Color.white;
    private Color m_PreHoverColor = Color.white;
    private Color m_TargetColor = Color.green;
    private float m_TrailTime = 0.0f;

    public bool IsReady => m_IsReady;
    public TileType Type => m_TileType;
    public Vector2Int GridPosition => m_GridPosition;

    // Update is called once per frame
    void Update()
    {
        if (!m_IsHovered)
        {
            if (m_TrailTime > 0.0)
            {
                meshRenderer.material.color = Color.Lerp(m_OriginalColor, m_TargetColor, m_TrailTime);
                m_TrailTime -= Time.deltaTime;
            }
            else
            {
                meshRenderer.material.color = m_OriginalColor;
            }
        }
    }

    public void SetupTile(int x, int y, TileType type)
    {
        m_GridPosition = new Vector2Int(x, y);
        m_TileType = type;
        m_TileName = $"Tile_{x}_{y}";
        if (type == TileType.Obstacle)
            obstacleObject.SetActive(true);
        else
            obstacleObject.SetActive(false);
        meshRenderer.material.color = m_OriginalColor;
        m_IsReady = true;
    }

    public string GetInfo()
    {
        return $"Tile {m_TileName} at {m_GridPosition} is of type {m_TileType}";
    }

    public void SetHovered(bool isHovered)
    {
        if (!m_IsHovered && isHovered) m_PreHoverColor = meshRenderer.material.color;
        m_IsHovered = isHovered;
        meshRenderer.material.color = isHovered ? hoveredColor : m_PreHoverColor;
    }

    public void SetColor(Color color)
    {
        m_TargetColor = color;
        m_TrailTime = 1.0f;
    }
}
